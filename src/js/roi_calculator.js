(function ($) {
    $('.form-calculator').on('submit', generate);
})(jQuery);

function generate() {
    //Monthly Budget
    var budget = parseInt(jQuery('#monthly-budget').val());
    
    // Monthly Leads
    var leads_now = Math.round((budget / 2) / 50),
        leads_6months = Math.round(leads_now * 1.7),
        leads_12months = Math.round(leads_now * 3.5),
        leads_24months = Math.round(leads_now * 7.2);

    //Annual rev increase
    var rev_now = (leads_now * parseFloat(jQuery('#monthly-sales').val())) * 12,
        rev_affected = rev_now * 3.5;


        jQuery('#leads-1').html(leads_now + " qualified leads");
        jQuery('#leads-12').html(leads_12months + " qualified leads");
        jQuery('.roi-revenue span').html(rev_affected.formatMoney(0));

    zingchart.render({
        id: 'roi-leads-chart',
        width: '100%',
        data: {
            "type": "bar",
            "title": {
                text: 'Potential Monthly Leads'
            },
            "scale-x": {
                "labels": ["1 month", "6 months", "12 months", "24 months"]
            },
            "plot": {
                "animation": {
                    "delay": "100",
                    "effect": "4",
                    "method": "5",
                    "sequence": "1"
                },
                valueBox: {
                    text: '%v',
                    color: '#606060',
                    textDecoration: 'underline'
                },
                styles: [
                    "gray",
                    "#29C3EC",
                    "#29C3EC",
                    "#29C3EC"
                ]
            },
            "series": [{
                "values": [leads_now, leads_6months, leads_12months, leads_24months],
            }]
        },
    });
    jQuery(document).on('click', '.roi-toggle-button', function () {
        var $this = jQuery(this);
        if (!$this.hasClass('is--active')) {
            if ($this.data('roi-type') == 'volume') {
                zingchart.exec('roi-leads-chart', 'setseriesvalues', {
                    values: [
                        [leads_now, leads_6months, leads_12months, leads_24months]
                    ]
                });
            } else {
                zingchart.exec('roi-leads-chart', 'setseriesvalues', {
                    values: [
                        [1, 1.7, 3.5, 7.2]
                    ]
                });
            }
            $this.addClass('is--active').siblings().removeClass('is--active');
        }
    });

    jQuery('#roi-results').fadeIn(function () {
        jQuery('html, body').animate({
            scrollTop: jQuery("#roi-results").offset().top
        }, 1000);
    });

    return false;
}

Number.prototype.formatMoney = function (x, y, z) {
    var n = this,
        c = isNaN(x = Math.abs(x)) ? 2 : x,
        d = y == undefined ? "." : y,
        t = z == undefined ? "," : z,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};