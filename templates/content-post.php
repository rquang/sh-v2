<div class="blog-post block-transition">
    <a class="blog-post-image" href='<?php the_permalink(); ?>'>
        <?php if (has_post_thumbnail()) : ?>
            <img src="<?php echo get_the_post_thumbnail_url(); ?>">
        <?php endif; ?>
    </a>
    <div class="blog-post-meta">
        <a class='blog-post-title underline' href='<?php the_permalink(); ?>'>
            <h3><?php the_title(); ?></h3>
        </a>
        <div class="blog-post-details">
            <span class='blog-post-date'>
                <?php the_time(get_option('date_format')); ?>
            </span>
            <div class="blog-post-category">
                <?php the_category( ' / ' ); ?>
            </div>
        </div>
        <div class='blog-post-content'>
            <?php the_excerpt(); ?>
        </div>
        <a class="blog-post-link secondary-button" href='<?php the_permalink(); ?>'>read more</a>
    </div>
</div>