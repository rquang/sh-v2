<div id="website-calculator" class="form-calculator">
    <div class="first">
        <label for="months">Select your Term:</label>
        <div class="months is--active" data-months="12">
            <span class="calculator-btn">12 Months</span>
        </div>
        <div class="months" data-months="24">
            <span class="calculator-btn">24 Months</span>
        </div>

        <div class="months" data-months="36">
            <span class="calculator-btn">36 Months</span>
        </div>
    </div>
    <div class="second">
        <div class="package">
            <label for="package">Which Package are you Interested In?</label>
            <select name="package" id="packages">
                <option value="0">Please select a Package</option>
                <option value="599.99">Landing Page Website $599.99</option>
                <option value="1499.99">Basic Website $1499.99</option>
                <option value="4999.99">Corporate Website $4999.99</option>
                <option value="3499.99">ECommerce Website $3499.99</option>
            </select>
        </div>
        <div class="amount">
            <div class="wrapper">
                <h3>Your Monthy Payments:</h3>
                <span id="monthly-payment">0</span>
            </div>
        </div>
    </div>
    <div class="disclaimer">
        <ul>
            <li>Please note that these prices are only an estimate.</li>
            <li>Minimum 20% down payment required on 24 and 36 month terms.</li>
            <li>Packages less than $1000 do not qualify for terms longer than 12 months.</li>
        </ul>
    </div>
</div>

<script>
    (function ($) {

        $(document).ready(function ($) {

            $('#website-calculator .months').on('click', function () {

                $this = $(this);

                if (!$this.hasClass('is--active'))

                    $('#website-calculator .months').removeClass('is--active');

                $this.addClass("is--active");

                calculateMonths();

            });

            $('#packages').change(calculateMonths);

            function calculateMonths() {

                var term = $('#website-calculator .months.is--active').data('months'),

                    package = $('#packages').val(),

                    amount = Math.round(package / term);

                $('#monthly-payment').html(amount);

            }

        });

    })(jQuery);
</script>