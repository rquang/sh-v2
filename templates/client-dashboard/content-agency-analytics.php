<div id="sh-analytics" class="client-dashboard-section">
    <iframe src="<?php echo aa_get_token(); ?>" frameborder="0" height="1000px"></iframe>
</div>

<script>
    (function ($) {
        $('#sh-analytics iframe').load(function () {
            $(this).contents().find('.common-menu-sidebar').remove();
            $(this).contents().find('.common-header').remove();
        });
    })(jQuery);
</script>