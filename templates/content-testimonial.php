<div class="testimonial-post item-video">
    <div class="testimonial-post-video">
        <a class="owl-video" href="<?php the_field('youtube_url'); ?>"></a>
    </div>
    <div class="testimonial-post-meta">
        <h4 class="testimonial-post-name"><?php the_field('full_name'); ?></h4>
        <p class="testimonial-post-company"><?php the_field('company'); ?></p>
    </div>
</div>